import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import MapView from 'react-native-maps';
import Maps2 from './Maps2'
import Directions from './Search'
export default class Home extends Component {
    state = {
        region: null,
        destination: null
    };
    async componentDidMount() {
        this._isMounted = true;
        navigator.geolocation.getCurrentPosition(
            ({ coords: { latitude, longitude } }) => {
                const region = {
                    latitude,
                    longitude,
                    latitudeDelta: 0.0143,
                    longitudeDelta: 0.0134
                };
                if (this._isMounted) this.setState({ region, regionSet: true });
            },
            error => alert(JSON.stringify(error)),
            {
                timeout: 2000,
                enableHighAccuracy: true,
                // maximumAge: 1000
            }
        );
    }
    handleLocationSelected_ = (data, { geometry }) => {
        const { location: { lat: latitude, lng: longitude } } = geometry;
        alert("ACHEI");
        this.setState({
            destino: {
                latitude,
                longitude,
                title: data.structured_formatting.main_text,
            },
        })
        alert(destino)
    }
    render() {
        const { region, destino } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <MapView
                    style={styles.map}
                    loadingEnabled={true}
                    showsUserLocation={true}
                    initialRegion={region}>
                    {destino && (
                        <Directions
                            origin={region}
                            destination={destino}
                            onReady={() => { }}
                        />
                    )}
                </MapView>
                <Maps2
                    onlocationSelected={this.handleLocationSelected}
                />
            </View>
        );
    }
    handleLocationSelected = () => {
        alert("OIBB");
    }
}
const styles = StyleSheet.create({
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    }
});
