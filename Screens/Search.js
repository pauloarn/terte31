import React from 'react';
import MapViewDirection from 'react-native-maps-directions';
const Directions = ({ destination, origin, onReady }) => (
    <MapViewDirection
        origin={origin}
        destination={destination}
        onReady={onReady}
        apikey="AIzaSyBul5dNanqTLIPimuZ74xKFcBCd7wtreZ0"
        strokeWidth={3}
        strokeColor="#222"
    />
);
export default Directions;
