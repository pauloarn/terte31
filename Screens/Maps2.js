import React, { Component } from 'react';
import { Platform } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
export default class Maps2 extends Component {
    render() {
        const { onLocationSelected } = this.props;
        return (
            <GooglePlacesAutocomplete
                placeholder="Qual seu destino?"
                placeholderTextColor="#333"
                onPress={onLocationSelected}
                query={{
                    key: "AIzaSyBul5dNanqTLIPimuZ74xKFcBCd7wtreZ0",
                    language: "pt"
                }}
                textInputProps={{
                    autoCapitalize: "none",
                    autoCorrect: false
                }}
                fetchDetails
                enablePoweredByContainer={false}
                styles={{
                    container: {
                        position: 'absolute',
                        top: Platform.select({ ios: 60, android: 40 }),
                        width: "100%"
                    },
                    textInputContainer: {},
                    textInput: {},
                    listView: {},
                    description: {},
                    row: {}
                }}
            />
        )
    }
}
