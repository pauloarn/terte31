import React, { Component } from 'react'
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import bgImage from '../images/bgImage.jpg';
const { width: WIDTH } = Dimensions.get('window');
export default class Cadastro extends Component {
    render() {
        return (
            <ImageBackground source={bgImage} style={styles.backGroundContainer}>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.input}
                        placeholder={'NOME'}
                        placeholderTextColor={'rgba(255,255,255,0.7)'}
                        underlineColorAndroid='transparent'
                       /* onChangeText={(user) => this.setState({ user })} */ />
                </View>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.input}
                        placeholder={'CPF'}
                        placeholderTextColor={'rgba(255,255,255,0.7)'}
                        keyboardType='numeric'
                        underlineColorAndroid='transparent'
                       /* onChangeText={(user) => this.setState({ user })} */ />
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backGroundContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputContainer: {
        marginTop: 20,
    },
    input: {
        width: WIDTH - 55,
        height: 45,
        borderRadius: 10,
        fontSize: 16,
        paddingLeft: 25,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        marginHorizontal: 25,
        alignItems: 'center',
    },
});
