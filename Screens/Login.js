import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import bgImage from '../images/bgImage.jpg';
import logo from '../images/StayParty.png';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Actions } from 'react-native-router-flux';
const { width: WIDTH } = Dimensions.get('window');
export default class Login extends Component {
  constructor() {
    super()
    this.state = {
      showPass: true,
      press: false,
      user: '',
      password: '',
    }
  }
  showPass = () => {
    if (this.state.press == false) {
      this.setState({ showPass: false, press: true })
    } else {
      this.setState({ showPass: true, press: false })
    }
  }
  render() {
    return (
      <ImageBackground source={bgImage} style={styles.backGroundContainer}>
        <View style={styles.logoContainer}>
          <Image source={logo} style={styles.logo} />
        </View>
        <View style={styles.inputContainer}>
          <Icon name={'account-outline'} size={28} color={"rgba(255,255,255,0.7)"}
            style={styles.inputIcon} />
          <TextInput
            style={styles.input}
            placeholder={'LOGIN'}
            placeholderTextColor={'rgba(255,255,255,0.7)'}
            underlineColorAndroid='transparent'
            onChangeText={(user) => this.setState({ user })} />
        </View>

        <View style={styles.inputContainer}>
          <Icon name={'lock-outline'} size={28} color={"rgba(255,255,255,0.7)"}
            style={styles.inputIcon} />
          <TextInput
            style={styles.input}
            placeholder={'SENHA'}
            secureTextEntry={this.state.showPass}
            placeholderTextColor={'rgba(255,255,255,0.7)'}
            underlineColorAndroid='transparent'
            onChangeText={(password) => this.setState({ password })} />

          <TouchableOpacity style={styles.btnEye} onPress={this.showPass.bind(this)}>
            <Icon name={this.state.press == false ? 'eye-outline' : 'eye-off-outline'} size={26} color={'rgba(255,255,255,0.7)'} />
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.btnLogin}
          onPress={(this.autentication)}>
          <Text style={styles.text}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.btnLogin}
          onPress={(this.gotoCadastro)}>
          <Text style={styles.text}>Cadastre-se</Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
  autentication = () => {
    if (this.state.user === "Admin" && this.state.password === "Admin") {
      Actions.Home();
    } else {
      alert("Credencial Incorreta")
    }
  }
  gotoCadastro = () => {
    Actions.Cadastro();
  }
}

const styles = StyleSheet.create({
  logo: {
    width: 220,
    height: 200,
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0,
  },
  backGroundContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputContainer: {
    marginTop: 20,
  },
  input: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 10,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0,0,0,0.35)',
    color: 'rgba(255,255,255,0.7)',
    marginHorizontal: 25,
    alignItems: 'center',
  },
  inputIcon: {
    position: "absolute",
    left: 37,
    top: 5

  },
  btnEye: {
    position: "absolute",
    right: 45,
    top: 7
  },
  text: {
    color: 'rgba(255,255,255,0.7)',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  btnLogin: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    backgroundColor: 'rgb(150, 35, 186)',
    justifyContent: 'center',
    marginTop: 20,

  }
});
