import React, { Component } from 'react';
import { View, Text, Image, Platform } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
export const GooglePlacesInput = () => {
    return (
        <GooglePlacesAutocomplete
            placeholder="Qual seu destino?"
            placeholderTextColor="#333"
            onPress={() => { }}
            query={{
                key: "AIzaSyBul5dNanqTLIPimuZ74xKFcBCd7wtreZ0",
                language: "pt"
            }}
            textInputProps={{
                autoCapitalize: "none",
                autoCorrect: false
            }}
            fetchDetails
            enablePoweredByContainer={false}
            styles={{
                container: {
                    position: 'absolute',
                    top: Platform.select({ ios: 60, android: 40 }),
                    width: "100%"
                },
                textInputContainer: {},
                textInput: {},
                listView: {},
                description: {},
                row: {}
            }}
        />

    )
}