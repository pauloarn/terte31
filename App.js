import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Text,
	View,
} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import Login from './Screens/Login';
import Home from './Screens/Home';
import Cadastro from './Screens/Cadastro';
export default class App extends Component {
	render() {
		return (
			<Router>
				<Scene key="root">
					<Scene key="Login" component={Login}
						hideNavBar />
					<Scene key="Home" component={Home}
						hideNavBar
						initial />
					<Scene key="Cadastro" component={Cadastro}
						hideNavBar />
				</Scene>
			</Router>
		);
	}
}
